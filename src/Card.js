import React from 'react';
import { FaRegHeart, FaHeart } from "react-icons/fa";
const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const CardComponent = (props) => {
  const { id, host_name, country, bedrooms, bathrooms, price, last_scraped, calendar_last_scraped } = props.details;
  const date1 = new Date(last_scraped);
  const date2 = new Date(calendar_last_scraped);

  return (
    <div className="card shadow-sm h-100 px-3 bg-white rounded border-0">
      <div className="card-body">
        <div className="card-title"><div className="d-flex justify-content-between">
          <h6>{`${host_name}, ${country}`} </h6>
          <div className="fs-8 ml-auto">
            {(props.lowRate.id === id) && <button type="button" className="btn btn-purple btn-space">Low rate</button>}
            {(!bedrooms && props.lowRate.id !== id) && <button type="button" className="btn btn-blue btn-space">Usually Booked</button>}
            {id % 3 ? <FaRegHeart /> : <FaHeart />}
          </div>
        </div></div>
        <div className="text-muted">{`${bedrooms} bedrooms, ${bathrooms} bathrooms`}</div>
        <div className="text-muted mb-0">  {months[date1.getMonth()] + " " + date1.getDate()} -  {months[date2.getMonth()] + " " + date2.getDate()} </div>
      </div>
      <div className="card-footer bg-white">
        <div className="d-flex justify-content-between text-muted">
          <div>Total</div>
          <div className="ml-auto">${price}</div>
        </div>
      </div>
    </div>
  );
}

export default CardComponent;
