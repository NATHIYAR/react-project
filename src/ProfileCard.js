import React from 'react';

const profileCard = (props) => {
  return (<>
    <div className="circle" onClick={props.handleClick}>JS</div>
    {props.show && <div className="card profile shadow-sm px-3 bg-white rounded border-0" style={{ width: "25rem" }}>
      <div className="card-body">
        <h6 className="card-title">Jane Smith</h6>
        <div className="text-muted">jane.smit@me.com </div>
        <div className="d-flex justify-content-between text-muted pt-4">
          <a className="link-opacity-10-hover text-dark fw-semibold" href="#">View Profile</a>
          <button className="btn btn-signout rounded-pill  ml-auto" type="submit">Sign out</button>
        </div>
      </div>
    </div>}
  </>)
}

export default profileCard;