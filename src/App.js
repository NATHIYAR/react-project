import React, { useEffect, useState } from 'react';
import CardComponent from './Card';
import ProfileCard from './ProfileCard';
import { GoHome } from "react-icons/go";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

const CardGrid = () => {
  const [show, setShow] = useState(false);
  const [places, setPlaces] = useState([]);
  const [currentPagination, setCurrentPagination] = useState(0);
  const maxItemsPerPage = 9;

  useEffect(() => {
    fetchData();
  }, [])

  // fetch data from an API
  async function fetchData() {
    try {
      const response = await fetch('https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/airbnb-listings/records'); // Initiating the HTTP request
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setPlaces(data.results);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }

  const handleClick = () => {
    setShow(!show);
  }

  // get lowest price object from places
  const lowRate = places?.reduce((acc, room) => {
    if (!acc.price) acc = room;
    else if (room.price < acc.price) acc = room;
    return acc;
  }, {})

  const startingIndex = currentPagination * maxItemsPerPage;
  const endingIndex = currentPagination + maxItemsPerPage;
  const homesList = places?.slice(startingIndex, endingIndex) || [];

  return (
    <div className='mx-4'>
      <header>
        <div className="d-flex justify-content-between py-3">
          <div className='home-color fw-semibold'> <GoHome /> Acme Homes</div>
          <div className='ml-auto'>
            <ProfileCard handleClick={handleClick} show={show} />
          </div>
        </div>
      </header>
      <h5 className="fs-7 mb-4">Available Stays</h5>
      <div className="row g-4">
        <h6> Places you might like</h6>
        {homesList.map((item, index) => (
          <>{index === 3 && <h6> Nearby stays</h6>}
            <div className="col-md-4" key={index}><CardComponent details={item} lowRate={lowRate} /></div>
          </>
        ))}
      </div>
      <footer className="border-top mt-4 p-3">
        <div className="d-flex justify-content-between">
          <div className="text-muted">{`Showing ${startingIndex + 1} - ${endingIndex} of ${places.length}`}</div>
          <div className="ml-auto">
            <button type="button" className="btn btn-outline-secondary btn-gray btn-space" onClick={() => setCurrentPagination(currentPagination - 1)} disabled={currentPagination === 0}>Previous</button>
            <button type="button" className="btn btn-outline-secondary btn-gray ml-4" onClick={() => setCurrentPagination(currentPagination + 1)} disabled={places.length === endingIndex}>Next</button>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default CardGrid;
